<?php
header('Content-type: text/plain');

class API {
    public function printFullName($name) {
      echo "Full Name: $name \n";
    }
  
    public function printHobbies(array $hobbies) {
      echo "Hobbies: " . implode(", ", $hobbies) . "\n";
    }
  
    public function printPersonalInfo(object $info) {
      echo "Age: $info->age\n";
      echo "Email: $info->email\n";
      echo "Birthday: $info->birthday\n";
    }
  }
  
  // usage
  $api = new API();
  $api->printFullName("Ralph Marvin Ayala");
  $api->printHobbies(["\n    Basketball\n    coding\n    biking"]);
  $api->printPersonalInfo((object) [
    "age" => 23,
    "email" => "ralphmarvin.ayala.dev.pixel8@example.com",
    "birthday" => "1999-06-27"
  ]);
?>


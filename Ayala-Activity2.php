<?php
require_once 'vendor/autoload.php';

use MysqliDb;

class Form {
  private $conn;
  private $table = "pinfo";

  public function __construct() {
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "activity2";
    

    // Create connection
    $this->conn = new mysqli($servername, $username, $password, $dbname);

    // Check connection
    if ($this->conn->connect_error) {
        die("Connection failed: " . $this->conn->connect_error);
    }
  }

  public function createForm() {
    if (isset($_POST['submit'])) {
        $ids = $_POST['ids'];
      $full_name = $_POST['full_name'];
      $age = $_POST['age'];
      $address = $_POST['address'];

      $sql = "INSERT INTO $this->table (ID, Name, Age, Addres) VALUES ('$ids', '$full_name', '$age', '$address')";

      if ($this->conn->query($sql) === TRUE) {
          echo "<alert>New record created successfully</alert>";
          header("Location: Ayala-Activity2.php");
      } else {
          echo "Error: " . $sql . "<br>" . $this->conn->error;
          exit();
      }
    }


    ?>

    <form method="post">
    <input type="text" name="ids" value="<?php ?>" placeholder="ID" required><br>
      <input type="text" name="full_name" value="<?php  ?>" placeholder="Full Name" required><br>
      <input type="number" name="age" value="<?php ?>" placeholder="Age" required><br>
      <input type="text" name="address" value="<?php  ?>" placeholder="Address" required><br>
      <input type="submit" name="submit" value="Submit" style='background-color: blue; color: white;'>
    </form>

    <?php
  }
    public function deleteData() {
        if (isset($_POST['delete'])) {
          $id = $_POST['id'];
        mysqli_query($this->conn, "DELETE FROM $this->table where ID = $id");
        
       
        }
    }

    public function updates(){

        if (isset($_POST['edit'])) {
            $id = $_POST['id'];
            $full_name = $_POST['name'];
            $age = $_POST['age'];
            $address = $_POST['address'];
        
            $sql = "UPDATE $this->table SET Name='$full_name', Age=$age, Addres='$address' WHERE ID=$id";
            if (mysqli_query($this->conn, $sql)) {
                echo "Record updated successfully.";
                
                exit();
            } else {
                echo "Error updating record: " . mysqli_error($conn);
            }
        }
    }
    

    public function displayData() {
    $sql = "SELECT * FROM $this->table";
    $result = $this->conn->query($sql);
    if ($result->num_rows > 0) {

      echo "<table><tr><th>ID</th><th>Full Name</th><th>Age</th><th>Address</th><th>Status</th></tr>";
      // output data of each row
      while($row = $result->fetch_assoc()) {
       
        ?>
            
          <tr>
            <form method='post'>
            <td><input type='text' name='id' value="<?php echo $row['ID'];?>" placeholder='ID' required></input> <br></td>
            <td><input type='text' name='name' value="<?php echo $row['Name'];?>" placeholder='Name' required></input><br> </td>
            <td><input type='text' name='age' value="<?php echo $row['Age'];?>" placeholder='Age' required></input><br> </td>
            <td><input type='text' name='address' value="<?php echo $row['Addres'];?>" placeholder='Address' required></input><br> </td>
            
            <td>
            <button class='btn' style='background-color: green; color: white;' name='edit'>Update</button>
            <button class='btn' style='background-color: red; color: white;'name='delete'>Delete</button>
            </form>
            </td>
          </tr>

         
          <?php
      }
      echo "</table>";
    } else {
        echo "0 results";
    }

    
  }
}





$form = new Form();
$form->createForm();
$form->displayData();
$form->deleteData();
$form->updates();
?>
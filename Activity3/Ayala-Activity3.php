<?php
require  "../vendor/thingengineer/mysqli-database-class/MysqliDb.php";

class Form {
  private $conn;
  private $table = "pinfo";

  
  public function __construct() {
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "activity2";

    // Create connection
    $this->conn = new MysqliDb ($servername, $username, $password, $dbname);
  }

  public function createForm() {
    if (isset($_POST['submit'])) {
      $ids = $_POST['ids'];
      $full_name = $_POST['full_name'];
      $age = $_POST['age'];
      $address = $_POST['address'];

      $data = array(
        'ID' => $ids,
        'Name' => $full_name,
        'Age' => $age,
        'Addres' => $address,
      );
      $id = $this->conn->insert($this->table, $data);
      if ($id) {
        echo "<alert>New record created successfully</alert>";
        header("Location: Ayala-Activity3.php");
      } else {
        echo "Error: " . $this->conn->getLastError();
        exit();
      }
    }


    ?>

    <form method="post">
    <input type="text" name="ids" value="" placeholder="ID" required><br>
      <input type="text" name="full_name" value="" placeholder="Full Name" required><br>
      <input type="number" name="age" value="" placeholder="Age" required><br>
      <input type="text" name="address" value="" placeholder="Address" required><br>
      <input type="submit" name="submit" value="Submit" style='background-color: blue; color: white;'>
    </form>

    <?php
  }
  public function deleteData() {
    if (isset($_POST['delete'])) {
      $id = $_POST['id'];
      $this->conn->where('ID', $id);
      if ($this->conn->delete($this->table)) {
        echo "Record deleted successfully.";
      } else {
        echo "Error: " . $this->conn->getLastError();
      }
    }
  }

  public function updates(){
    if (isset($_POST['edit'])) {
      $id = $_POST['id'];
      $full_name = $_POST['name'];
      $age = $_POST['age'];
      $address = $_POST['address'];
      $data = array(
        'Name' => $full_name,
        'Age' => $age,
        'Addres' => $address,
      );
      $this->conn->where('ID', $id);
      if ($this->conn->update($this->table, $data)) {
        echo "Record updated successfully.";
      } else {
        echo "Error updating record: " . $this->conn->getLastError();
      }
    }
  }

  public function displayData() {
    $rows = $this->conn->get($this->table);

    if ($this->conn->count > 0) {

      echo "<table><tr><th>ID</th><th>Full Name</th><th>Age</th><th>Address</th><th>Status</th></tr>";
      // output data of each row
      foreach ($rows as $row) {
       
        ?>
            
          <tr>
            <form method='post'>
            <td><input type='text' name='id' value="<?php echo $row['ID'];?>" placeholder='ID' required></input> <br></td>
            <td><input type='text' name='name' value="<?php echo $row['Name'];?>" placeholder='Name' required></input><br> </td>
            <td><input type='text' name='age' value="<?php echo $row['Age'];?>" placeholder='Age' required></input><br> </td>
            <td><input type='text' name='address' value="<?php echo $row['Addres'];?>" placeholder='Address' required></input><br> </td>
            
            <td>
            <button class='btn' style='background-color: green; color: white;' name='edit'>Update</button>
            <button class='btn' style='background-color: red; color: white;'name='delete'>Delete</button>
            </form>
            </td>
          </tr>

         
          <?php
      }
      echo "</table>";
    } else {
        echo "0 results";
    }

    
  }
}





$form = new Form();
$form->createForm();
$form->displayData();
$form->deleteData();
$form->updates();
?>


